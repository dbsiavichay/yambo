from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
	cellphone = models.CharField(max_length=16, blank=True, null=True)
	birthday = models.DateField()
	gender = models.PositiveSmallIntegerField()
	image = models.ImageField(upload_to='profile_images', blank=True, null=True)
	can_create_spot = models.BooleanField(default=False)
	requests = models.ManyToManyField('self')
	user = models.OneToOneField(User)

	def __unicode__(self):
		return '%s %s' % (self.user.first_name, self.user.last_name)

class Friendship(models.Model):
	is_active = models.BooleanField(default=True)
	date_joined = models.DateTimeField(auto_now=True)
	user_profile = models.ForeignKey(Profile, related_name='friends')
	friend_profile = models.ForeignKey(Profile)

	def __unicode__(self):
		state = '<==>' if self.is_active else '<=/=>'
		return '%s %s %s' % (self.user_profile, state, self.friend_profile)

