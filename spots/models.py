from django.db import models
from relationships.models import Profile

class Category(models.Model):
	name = models.CharField(max_length=32)

	def __unicode__(self):
		return self.name

class Spot(models.Model):
	name = models.CharField(max_length=64)
	address = models.CharField(max_length=128)
	latitude = models.FloatField()
	longitude = models.FloatField()
	logo_image = models.ImageField(upload_to='logos', blank=True, null=True)
	front_image = models.ImageField(upload_to='fronts', blank=True, null=True)
	is_active = models.BooleanField(default=True)
	date_joined = models.DateTimeField(auto_now=True)
	categories = models.ManyToManyField(Category)

	def __unicode__(self):
		return self.name

class Follower(models.Model):
	is_owner = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	date_joined = models.DateTimeField(auto_now=True)
	spot = models.ForeignKey(Spot)
	profile = models.ForeignKey(Profile)

	def __unicode__(self):
		return '%s ==> %s' % (self.profile, self.spot)


